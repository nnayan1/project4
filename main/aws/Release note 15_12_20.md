
Dev center
Ver: 2.0.1
Date : 15/12/20
Fixes with this patch release
Create button will be disabled on create event popup with only spaces in the text fields.
Popup overlay will cover the entire screen.
UI issue related to secondary header after creating a doc has been resolved.
%20%20 will not appear in the URLs.
UI issue on dev journey has been be resolved.
Event title issue on email template has been resolved.
Publish option will be available for the trusted authors on CC.ee
check
Improvements
